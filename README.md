<p align="center">
    <h1 align="center">Task e-queo.com</h1>
</p>

#Задача
<p>   
Необходимо создать микросервис, который в качестве своего единственного параметра принимает любой файл PDF-формата и затем сохраняет его в S3-совместимое хранилище.
В результате выполнения сервис должен вернуть либо URL загруженного файла в хранилище, либо - соответствующую ошибку.
</p>

#Условия
<p>
Аутентификация/авторизация не требуется
Можно использовать любой фреймворк и библиотеки
Язык реализации - PHP, но допустимы варианты с Java  /JavaScript / Go
Архитектура - REST / gRPC
</p>

#Требования к сервису
<p>
Надежность
</p>


<p>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
</p>


```
git clone https://gitlab.com/tasks4/task_e-queo.com.git 
```

```
php init
```

```
composer install
```
```
docker-compose up --build
```

<p>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
</p>

<p>
<img src="./frontend/web/images/Screenshot from 2021-12-12 22-11-14.png" height="500px">
</p>

<p>
<img src="./frontend/web/images/Screenshot from 2021-12-12 22-11-22.png" height="500px">
</p>

<p>
<img src="./frontend/web/images/Screenshot from 2021-12-12 22-11-54.png" height="500px">
</p>

<p>
<img src="./frontend/web/images/Screenshot from 2021-12-12 22-12-03.png" height="500px">
</p>

<p>
<img src="./frontend/web/images/Screenshot from 2021-12-12 22-12-37.png" height="500px">
</p>

