<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/**
 * @var $model \frontend\models\UploadForm
 * @var $listObjects array
 */

Yii::$app->name = 'Task e-queo.com'
?>

<div class="container">

    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

    <div class="col-xs-12">
        <?php

        $options = [];
        $imgPath = [];

        $uploadFileUrl = Url::to(['/main/upload-files']);

        if ($listObjects) {
            $imgFullPath = Yii::getAlias("@frontend") . "/main/file?filename=";

            foreach ($listObjects as $object) {
                $fileName = $object['key'];
                $deleteUrl = Url::to(["/main/delete-file?filename=" . $fileName]);
                $imgPath[] = Url::to('http://' . $_SERVER['HTTP_HOST'] . '/main/file?filename=') . $fileName;

                $options[] =
                    [
                        'caption' => $fileName,
                        'url' => $deleteUrl,
                        'size' => $object['size'],
                        'key' => $fileName,
                    ];
            }
        }
        ?>

        <?=
        $form->field($model, 'files[]')
            ->widget(
                FileInput::class,
                [
                    'attribute' => 'files[]',
                    'name' => 'files[]',
                    'options' => [
                        'accept' => 'application/pdf',
                        'multiple' => true,
                    ],
                    'pluginOptions' => [
                        'previewFileType' => 'any',
                        "uploadAsync" => true,
                        'showPreview' => true,
                        'showUpload' => true,
                        'showCaption' => false,
                        'showDrag' => true,
                        'uploadUrl' => $uploadFileUrl,
                        'initialPreviewConfig' => $options,
                        'initialPreview' => $imgPath,
                        'initialPreviewAsData' => true,
                        'initialPreviewShowDelete' => true,
                        'overwriteInitial' => true,
                        'resizeImages' => true,
                        'layoutTemplates' => [!$listObjects ?: 'actionUpload' => '',]
                    ],
                ]);
        ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
