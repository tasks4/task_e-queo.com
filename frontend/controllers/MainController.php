<?php

namespace frontend\controllers;

use frontend\models\UploadForm;
use yii\web\Controller;
use Yii;
use yii\web\Response;
use yii\web\UploadedFile;

class MainController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        Yii::$app->minio->setBucket('public');
        $model = new UploadForm();

        return
            $this->render(
                'index',
                [
                    'model' => $model,
                    'listObjects' => Yii::$app->minio->listObjects()
                ]
            );
    }

    /**
     * @param $filename
     * @return mixed
     */
    public function actionFile($filename)
    {
        Yii::$app->response->format = Response::FORMAT_RAW;

        $response = Yii::$app->getResponse();
        $response->headers->set('Content-Type', 'application/pdf');

        return Yii::$app->minio->read($filename);
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function actionUploadFiles()
    {
        $model = new UploadForm();
        $listFiles = UploadedFile::getInstances($model, "files");

        if (!empty($listFiles)) {
            foreach ($listFiles as $file) {
                Yii::$app->minio
                    ->writeStream(
                        sprintf("%s.%s", Yii::$app->security->generateRandomString(), $file->extension),
                        fopen($file->tempName, 'r+')
                    );
            }

            return true;
        }

        return false;
    }

    /**
     * @param $filename
     * @return bool
     */

    public function actionDeleteFile($filename)
    {
        $exists = Yii::$app->minio->has($filename);

        if ($exists) {
            Yii::$app->minio->delete($filename);

            return true;
        }

        return false;
    }
}