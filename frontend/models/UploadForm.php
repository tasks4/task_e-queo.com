<?php

namespace frontend\models;

use Yii;
use yii\base\Model;


class UploadForm extends Model
{
    /**
     * @var string
     */
    public $files;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['files',], 'required'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'files' => 'Загрузить',
        ];
    }
}
